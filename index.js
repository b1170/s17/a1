console.log("Hello");

let newStudent = [];

function addStudent(name) {
    newStudent.push(name)
    let lastStudent = newStudent.slice(-1);
    console.log(lastStudent + " was added to the student's list.")
}

addStudent("John");
addStudent("Jane");
addStudent("Joe");
addStudent("Damien");
addStudent("Maria")

function countStudents() {
    console.log("There are a total of " + newStudent.length + " students enrolled.");
}

countStudents();

function printStudents() {
    newStudent.sort();
    newStudent.forEach(student => {
        console.log(student);
    })
}

printStudents();


//console.warn("check here")



function findStudent (x){
 
    let searchLower = x.toLowerCase();
    let searchResult = newStudent.filter(student => student.toLowerCase().indexOf(searchLower) >-1 )

    if(searchResult != false) {
        console.log(searchResult + " is an Enrollee.");
    } else {
        console.log("No student found with the name " + x);
    }
}

findStudent('j');
findStudent('joe');
findStudent('bill');
